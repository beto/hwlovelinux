# Avaries de hardware

## Font d'alimentació

És la que proporciona energia a l'ordinador. Converteix els 220V de corrent alterna a corrent contínua.

![1614771068368](/home/beto/.config/Typora/typora-user-images/1614771068368.png)

Hi han de molts preus i mides. Les típiques son com la de la foto, una font ATX.

Per les averies influeix:

- La qualitat dels components de la font (si és una font molt económica és més probable que els components siguin dolents)
- Algunes fonts tenen un conmutador per seleccionar si la corrent d'entrada és 230V o 125V, important sempre a 230V
- Si la estabilitat de la tensió d'entrada és dolenta (hi han oscilacions degudes a la xarxa elèctrica de l'edifici o del barri) => això fa que moltes fonts s'espatllin
- Si hi han sobrecurrents, les fonts porten un fusible, però moltes vegades és inaccesible. 
- Habitualment quan una font no funciona es canvia per un altre
- Un altre problema que influeix perquè la font no funcioni adequadament és si tenim un cortcircuit en algun element connectat a la font. Per exemple:
  - Ports USB que fan cortcircuit entre dos pins o terminals. 



![1614771902378](/home/beto/.config/Typora/typora-user-images/1614771902378.png)

Ordre de resolució de incidències amb fonts:

- Revisar cables, fer probes amb un altre cable
- Treure els connectors de la placa base i mirar si arrenca fent cortcircuit entre el cable ver i negre

![1614772185810](/home/beto/.config/Typora/typora-user-images/1614772185810.png)

El pin 16 amb el cable de color verd, indica PS-on (Power Supply On), quan tu apretes el botó de l'ordinador el que estàs fent és cortociruitar el verd amb el negre ![1614772330256](/home/beto/.config/Typora/typora-user-images/1614772330256.png)

* Si la font no s'encén, no giran els ventiladors de la font, cal sustiuir la font

* Si és una font d'alimentació de portàtil, cal comprobar la tensió de sortida amb un tester:

  ![1614772561153](/home/beto/.config/Typora/typora-user-images/1614772561153.png)

* Si no hi ha tensió => cal canviar la font
* Si la font funciona, igualment la podem canviar per una de la mateixa potència, si funciona, potser que sigui la font que no va bé. 
* Si la font nova no funciona podem tindre un problema de cortcircuit o sobrecàrrega. 
  * Per exemple si connectem molts dispositius (disc durs, tarjetes gràfiques... ) potser no  dona suficient corrent-
  * The GeForce RTX 3080 is rated for 350W of total board power, and Nvidia recommends using a 750W power supply with it.
* És interessant fer la prova de l'rodinador amb el mínim de components i connectors per intentar esbrinar on tenim el problema de sobrecàrrega o cortcircuit



## Refrigeració de l'equip

Problemes:

- Els ventiladors amb el temps van més lents, tenen més fricció i més pols...
- ![1614773744628](/home/beto/.config/Typora/typora-user-images/1614773744628.png)

![1614774100088](/home/beto/.config/Typora/typora-user-images/1614774100088.png)

- con limpieza se puede quitar el polvo de radiadores y ventiladores:

  - Si lo haces con aire comprimido, nunca lo hagas desde poca distancia, puedes dañar los componentes
  - Para no deteriorarlos es buena idea usar un pincel

- si los ventiladores no giran o van muy despacio se han de cambiar...

- Si la temperatura del procesador de un PC sube por encima de los 90 grados, por precaución la bios corta la corriente

- En los portátiles es más complicado hacer sustituciones, pues cada modelo tiene su versión de refrigeración y su forma de hacer el recambio.

  ![1614774477505](/home/beto/.config/Typora/typora-user-images/1614774477505.png)

- A veces hay que cambiar la pasta térmica que pone en contacto el procesador con el disipador, es importante no poner un exceso de pasta térmica ya que puede generar averías en un futuro

![1614775080278](/home/beto/.config/Typora/typora-user-images/1614775080278.png)

Muy importante la limpieza de restos de pasta térmica y poner nueva en una cantidad justa (que no sobre)

## Disco duro

Tenemos una herramienta que nos permite monitorizar la "salud" del disco duro: SMART 

Un disco duro tiene una vida limitada, cuanto más escribes más cerca está de que finalice su vida útil. Los discos de gama "usuario" es más probable que se estropeen o fallen si se escribe mucho en ellos.

Si el disco es rotacional, una caída del equipo puede hacer que deje de funcionar.

Cuando tenemos un disco en un estado que "a veces arranca a veces no".  Es muy importante cuanto antes, si hay datos que conservar, hacer una copia. Si no se puede hacer una copia de todo el disco, se pueden intentar copias parciales con la herramienta "dd"

Si necesito recuperar ficheros que no están accesibles en una partición, lo puedo hacer con photorec.

Es interesante contar con una dockstation donde poder montar discos para montar el disco en un pc por usb (a ser posible usb3)

![1614776161789](/home/beto/.config/Typora/typora-user-images/1614776161789.png)

![1614776187907](/home/beto/.config/Typora/typora-user-images/1614776187907.png)

