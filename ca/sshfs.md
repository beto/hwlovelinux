Creamos dos máquinas virtuales:
- hd_rescue_fedora32 => donde recuperaremos los datos, será un escritorio basado en fedora_32_lxde_v1
- fedora_32_crashed => linux donde descargaremos imágenes, y borraremos la partición, después arrancaremos con system rescue cd para recuperar los datos

En la máquina fedora_32_lxde_v1, arrancamos en la red vlan241:
- verificamos que el sshd está corriendo y escuchando en el puerto,
- creamos directorio /opt/rescue
- cambiamos permisos para que el usuario isard pueda montar por sshfs

```
mkdir /opt/rescue
chown isard /opt/rescue
```

En la máquina fedora_32_crashed me descargo 3 imágenes:
```
wget https://upload.wikimedia.org/wikipedia/commons/0/01/Central_pyrenees.jpg
wget https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Pyrenees_topographic_map-ca.svg/1920px-Pyrenees_topographic_map-ca.svg.png
wget https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Pyrenees_Mountains_view_from_satellite.jpg/1280px-Pyrenees_Mountains_view_from_satellite.jpg
ls -lah *.jpg
ls -lah *.png
```

Miramos la partición donde esta montado el linux:
```
[isard@f32-isard ~]$ lsblk 
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
vda             252:0    0   60G  0 disk 
├─vda1          252:1    0    1G  0 part /boot
└─vda2          252:2    0   59G  0 part 
  ├─fedora-root 253:0    0   37G  0 lvm  /
  ├─fedora-swap 253:1    0  3.9G  0 lvm  [SWAP]
  └─fedora-home 253:2    0 18.1G  0 lvm  /home

```

Habremos de borrar /dev/vda2

- Reiniciamos con el system rescue cd en la red de clase (vlan 241)

```
parted -s /dev/vda print
parted -s /dev/vda rm 2
parted -s /dev/vda print
```

- Ahora montamos por sshfs, comprobamos que podemos acceder a la máquina de fedora por ssh, miramos la ip y accedemos:
```
ssh isard@10.200.241.46
```

no funciona, no me deja entrar como isard,

en el fedora revisamos la configuración del servicio sshd y añadimos un usuario:
```
[isard@f32-isard ~]$ sudo tail /etc/ssh/sshd_config
Subsystem	sftp	/usr/libexec/openssh/sftp-server

# Example of overriding settings on a per-user basis
#Match User anoncvs
#	X11Forwarding no
#	AllowTcpForwarding no
#	PermitTTY no
#	ForceCommand cvs server

AllowUsers root
```

Sólo tenía permisos root, añadimos isard con una línea al final del fichero de configuración:

```
sudo su -
echo "AllowUsers isard" >> /etc/ssh/sshd_config
systemctl restart sshd
```

Ahora ya nos deja acceder y montar el directorio remoto:
```
mkdir /mnt/rescue
sshfs isard@10.200.241.46:/opt/rescue /mnt/rescue
touch /mnt/rescue/prova
```

Y verificamos en fedora que se ve el fichero:
```
ls -lah /opt/rescue
```

