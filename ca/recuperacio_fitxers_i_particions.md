

Arranquem amb un windows, descarreguem imatges i les esborrem. ¿Les podem recuperar?

Fem boot amb un SRCD a la xarxa vlan 241 i podem accedir via xarxa amb aquest equip, desactivant el tallafocs i canviant el password de root:

```
iptables -P INPUT ACCEPT
iptables -F
passwd
```

Des d'una terminal podem accedir amb aquest equip:

```
#lsblk per coneixer quines particions tenim
[root@sysrescue ~]# lsblk 
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0    7:0    0 637.5M  1 loop /run/archiso/sfs/airootfs
sr0     11:0    1   699M  0 rom  /run/archiso/bootmnt
vda    254:0    0   120G  0 disk 
├─vda1 254:1    0   100M  0 part 
├─vda2 254:2    0    16M  0 part 
├─vda3 254:3    0 119.4G  0 part 
└─vda4 254:4    0   512M  0 part 
```

El disc és el /dev/vda que té 4 particions, per veure el tipus de sistema de fitxers de cada partición tenim:

```
[root@sysrescue ~]# wipefs /dev/vda1
DEVICE OFFSET TYPE UUID      LABEL
vda1   0x52   vfat 729E-6209 
vda1   0x0    vfat 729E-6209 
vda1   0x1fe  vfat 729E-6209 

```

Hi ha una partició de 100M tipus fat, el més normal és que aquí estiguin els fitxers de la partició UEFI

Podem fer un mount d'aquesta partició. Fem un directori on muntarem la partició:

```
mkdir /mnt/uefi_part
mount /dev/vda1 /mnt/uefi_part
```

Hi ha un directori EFI on està el gestor d'arranc de windows, 

```
[root@sysrescue ~]# ls -lh /mnt/uefi_part/EFI/Boot/
total 1.5M
-rwxr-xr-x 1 root root 1.5M Dec 24 08:31 bootx64.efi
```

A la segona partició el wipefs no troba res, és un sistema de fitxers que no reconeix o la partició no té cap sistema de fitxers, no està formatada:

```
wipefs /dev/vda2
```

A la tercera partició trobem una partició ntfs, que és on segurament trobarem un windows.

```bash
[root@sysrescue ~]# wipefs /dev/vda3
DEVICE OFFSET TYPE UUID             LABEL
vda3   0x3    ntfs 40BC9F2CBC9F1C0A 
```

Anem a muntar aquesta partició, fem un nou directori /mnt/windows i per muntar la partició podem escollir amb quin tipus de muntatge volem fer servir o deixem a mount que faci servir el que vulgui.

```
[root@sysrescue ~]# mkdir /mnt/windows

[root@sysrescue ~]# mount /dev/vda3 /mnt/windows
The disk contains an unclean file system (0, 0).
Metadata kept in Windows cache, refused to mount.
Falling back to read-only mount because the NTFS partition is in an
unsafe state. Please resume and shutdown Windows fully (no hibernation
or fast restarting.)

[root@sysrescue ~]# mount |grep /dev/vda3
/dev/vda3 on /mnt/windows type fuseblk (ro,nosuid,nodev,relatime,user_id=0,group_id=0,allow_other,blksize=4096)

```

Al fer el muntatge ha muntat tipus **fuseblk**. Fuse és un tipus de muntatge especial de linux per accedir a sistemes que no son nadius del kernel ([veure pag wikipedia](https://es.wikipedia.org/wiki/Sistema_de_archivos_en_el_espacio_de_usuario))

En realitat ha fet servir el software ntfs-3g per fer el muntatge:

```
[root@sysrescue ~]# umount  /dev/vda3
[root@sysrescue ~]# mount |grep /dev/vda3
[root@sysrescue ~]# mount -t ntfs-3g /dev/vda3 /mnt/windows
The disk contains an unclean file system (0, 0).
Metadata kept in Windows cache, refused to mount.
Falling back to read-only mount because the NTFS partition is in an
unsafe state. Please resume and shutdown Windows fully (no hibernation
or fast restarting.)
[root@sysrescue ~]# mount |grep /dev/vda3
/dev/vda3 on /mnt/windows type fuseblk (ro,nosuid,nodev,relatime,user_id=0,group_id=0,allow_other,blksize=4096)

```

El sistema està ***unclean***, que vol dir que pot tindre direccionament dels fitxers corrupte,  un fitxer que no s'ha tancat correctament, o una apagada brusca de l'ordinador.

És perillós treballar amb una partició per fer resize, escanejar i muntar si el sistema de fitxers no está "net". Amb la utilitat **ntfsfix** podem analitzar i netejar el sistema de fitxers

```
[root@sysrescue ~]# ntfsfix /dev/vda3
Mounting volume... The disk contains an unclean file system (0, 0).
Metadata kept in Windows cache, refused to mount.
FAILED
Attempting to correct errors... 
Processing $MFT and $MFTMirr...
Reading $MFT... OK
Reading $MFTMirr... OK
Comparing $MFTMirr to $MFT... OK
Processing of $MFT and $MFTMirr completed successfully.
Setting required flags on partition... OK
Going to empty the journal ($LogFile)... OK
Checking the alternate boot sector... OK
NTFS volume version is 3.1.
NTFS partition /dev/vda3 was processed successfully.

```

Ara muntem la partición de windows i anem a buscar la carpeta de Downloads de l'usuari. Als windows la carpeta de l'usuari està a c:\Users\

En el nostre linux ho tindrem a:

```bash
[root@sysrescue ~]# ls -l /mnt/windows/Users/
total 37
drwxrwxrwx 1 root root 8192 Dec 24 11:07  admin
lrwxrwxrwx 2 root root   24 Dec  7  2019 'All Users' -> /mnt/windows/ProgramData
drwxrwxrwx 1 root root 8192 Dec 24 06:15  Default
lrwxrwxrwx 2 root root   26 Dec  7  2019 'Default User' -> /mnt/windows/Users/Default
-rwxrwxrwx 1 root root  174 Dec  7  2019  desktop.ini
drwxrwxrwx 1 root root 8192 Dec 24 06:27  isard
drwxrwxrwx 1 root root 4096 Nov 18 23:48  Public
drwxrwxrwx 1 root root 8192 Dec 24 10:41  user

```

Mirem als diferents usuaris per trobar la carpeta de Downloads on estan les fotos:

```
[root@sysrescue ~]# ls /mnt/windows/Users/isard/Downloads/
 AutoLogon.zip   desktop.ini  'Firefox Installer.exe'
(failed reverse-i-search)`ldsc': ls /mnt/windows/Users/isard/Down^Cads/
[root@sysrescue ~]# ls /mnt/windows/Users/admin/Downloads/
desktop.ini  Valle-de-Benasque-2-768x512.jpg.webp

```

L'usuari que ens interesa es **admin** i volem recuperar aquesta foto

## Copiar fitxers a un dispositiu USB

Connectem el pendrive o el disc dur usb i mirem si el reconeix el linux:

```
[root@sysrescue ~]# lsblk 
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0    7:0    0 637.5M  1 loop /run/archiso/sfs/airootfs
sda      8:0    1  14.3G  0 disk 
└─sda1   8:1    1  14.3G  0 part 
sr0     11:0    1   699M  0 rom  /run/archiso/bootmnt
vda    254:0    0   120G  0 disk 
├─vda1 254:1    0   100M  0 part 
├─vda2 254:2    0    16M  0 part 
├─vda3 254:3    0 119.4G  0 part /mnt/windows
└─vda4 254:4    0   512M  0 part 

```

Apareix un nou dispositiu /dev/sda1 

El dmesg, que detecta els dispositius plug&play i assigna móduls del kernel per manegar aquests dispositius hem serveix per veure que ha passat realment:

```bash
[root@sysrescue ~]# dmesg -T |tail -n 20
[Tue Feb 16 09:30:55 2021] audit: type=1130 audit(1613467856.720:56): pid=1 uid=0 auid=4294967295 ses=4294967295 msg='unit=systemd-tmpfiles-clean comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=success'
[Tue Feb 16 09:30:55 2021] audit: type=1131 audit(1613467856.720:57): pid=1 uid=0 auid=4294967295 ses=4294967295 msg='unit=systemd-tmpfiles-clean comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=success'
[Tue Feb 16 09:34:09 2021] usb 2-4: new SuperSpeed Gen 1 USB device number 2 using xhci_hcd
[Tue Feb 16 09:34:09 2021] usb 2-4: New USB device found, idVendor=0781, idProduct=5591, bcdDevice= 1.00
[Tue Feb 16 09:34:09 2021] usb 2-4: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[Tue Feb 16 09:34:09 2021] usb 2-4: Product: Ultra USB 3.0
[Tue Feb 16 09:34:09 2021] usb 2-4: Manufacturer: SanDisk
[Tue Feb 16 09:34:09 2021] usb 2-4: SerialNumber: 4C530000260424115513
[Tue Feb 16 09:34:09 2021] usb-storage 2-4:1.0: USB Mass Storage device detected
[Tue Feb 16 09:34:09 2021] scsi host6: usb-storage 2-4:1.0
[Tue Feb 16 09:34:09 2021] usbcore: registered new interface driver usb-storage
[Tue Feb 16 09:34:09 2021] usbcore: registered new interface driver uas
[Tue Feb 16 09:34:10 2021] scsi 6:0:0:0: Direct-Access     SanDisk  Ultra USB 3.0    1.00 PQ: 0 ANSI: 6
[Tue Feb 16 09:34:10 2021] scsi 6:0:0:0: Attached scsi generic sg1 type 0
[Tue Feb 16 09:34:10 2021] sd 6:0:0:0: [sda] 30031872 512-byte logical blocks: (15.4 GB/14.3 GiB)
[Tue Feb 16 09:34:10 2021] sd 6:0:0:0: [sda] Write Protect is off
[Tue Feb 16 09:34:10 2021] sd 6:0:0:0: [sda] Mode Sense: 43 00 00 00
[Tue Feb 16 09:34:10 2021] sd 6:0:0:0: [sda] Write cache: disabled, read cache: enabled, doesn't support DPO or FUA
[Tue Feb 16 09:34:10 2021]  sda: sda1
[Tue Feb 16 09:34:10 2021] sd 6:0:0:0: [sda] Attached SCSI removable disk

```

A partir del missatge de les 9:34:09 podem veure que fa el kernel per treballar amb aquest dispositiu:

```
[Tue Feb 16 09:34:09 2021] usb 2-4: new SuperSpeed Gen 1 USB device number 2 using xhci_hcd	
```

Anem a mirar el sistema de fitxers d'aquesta partició del pendrive:

```
[root@sysrescue ~]# wipefs /dev/sda1
DEVICE OFFSET TYPE UUID      LABEL
sda1   0x52   vfat 7DC7-8BC5 
sda1   0x0    vfat 7DC7-8BC5 
sda1   0x1fe  vfat 7DC7-8BC5 
```

Es un tipus FAT, je el podem muntar:

```
[root@sysrescue ~]# mkdir /mnt/pendrive
[root@sysrescue ~]# mount /dev/sda1 /mnt/pendrive/
```

Ara ja podem crear un directori dins d'aquest pendrive i copiar la foto:

```
mkdir /mnt/pendrive/rescat
cp -a /mnt/windows/Users/admin/Downloads/Valle-de-Benasque-2-768x512.jpg.webp /mnt/pendrive/rescat/valle-venasque.jpg
```

Per extreure el pendrive millor si fem un umount:

```
umount /dev/sda1
```





