## Dispositius de emmagatzematge

Hi han 2 tecnologies per fabricar dispositius de emmagatzematge:
- ssd
- rotacionals


## Mapeig de dispositius al sistema de fitxers de linux

A linux "tot es un fitxer". Hi ha certes rutes que esten pensades per mapejar dispositius. 

El directori /dev/ fa referència a "device" (dispositiu)

Dispositius de block, amb prefix
- bus IDE (bus de disc molt antic): hd
-- /dev/hda
-- /dev/hdb

- bus SATA o USB (internament fa una conversió a SATA): sd
-- /dev/sda
-- /dev/sdb

- bus virtual de disk: vd
-- /dev/vda
-- /dev/vdb

# Eina parted per treballar amb les particions

Usage: parted [OPTION]... [DEVICE [COMMAND [PARAMETERS]...]...]

si faig servir parted sense opcions, entro en un terminal de parted:

