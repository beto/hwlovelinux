## Desde System Rescue CD

```
parted -s /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary 2MiB 500MB
parted -s /dev/vda mkpart primary 500MB 10GB
parted -s /dev/vda mkpart extended 10GB 100%
parted -s /dev/vda mkpart logical 10GB 12GB
parted -s /dev/vda mkpart logical 12GB 100%
```

Y con lsblk tenemos algo como esto:

![](../images/screenshot_1610440346.png)



Si queremos formatear y escribir datos en una partición de datos (vda6):

```
mkfs.ext4 /dev/vda6
mkdir /test
mount /dev/vda6 /test
dd if=/dev/random of=/test/fitxer_prova bs=1024 count=10240
umount /test
```



## Instalación de ubuntu - particiones

![](../images/screenshot_1610440538.png)

![](../images/screenshot_1610440553.png)

![](../images/screenshot_1610440591.png)

clic en cambiar y editar la partción:

![](../images/screenshot_1610440671.png)

En la partición 5 va la swap que se utiliza como área de intercambio:

![](../images/screenshot_1610440707.png)

Ha de quedar como:

![](../images/screenshot_1610440771.png)

![](../images/screenshot_1610440786.png)



## Instalación de debian - particiones





![](../images/screenshot_1610436403.png)











![](../images/screenshot_1610436518.png)



![](../images/screenshot_1610436625.png)

le damos al enter

![](../images/screenshot_1610436981.png)

![](../images/screenshot_1610437022.png)

Os tiene que quedar la primera partición algo parecido a esto:

![](../images/screenshot_1610437096.png)

Y guardamos los cambios

![](../images/screenshot_1610437149.png)

Segunda partición:

![](../images/screenshot_1610437192.png)

![](../images/screenshot_1610437229.png)

![](../images/screenshot_1610437247.png)

![](../images/screenshot_1610437291.png)

![](../images/screenshot_1610437324.png)

swap

![](../images/screenshot_1610437345.png)

![](../images/screenshot_1610437407.png)

Mantener los datos de ext3 de la partición 6:

![](../images/screenshot_1610437470.png)

![](../images/screenshot_1610437514.png)

HA DE QUEDAR AL FINAL:

![](../images/screenshot_1610437626.png)

![](../images/screenshot_1610437677.png)

![](../images/screenshot_1610437735.png)

