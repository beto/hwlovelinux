## Análisis forense de un disco con dd, sshfs y photorec

El escenario consta de dos máquinas virtuales de isard:

- CRASHED: máquina con un disco duro sin sistema operativo que arrancaremos con rescuecd

  - basada en **hism1_forensic_1**

    ![](../images/screenshot_1615280340.png)

  

- ANALISIS FORENSE: máquina de fedora 32 donde haremos el análisis del disco que extraemos de crashed

  - basada en **fedora 32 lxde v1** conectada a la red **vlan241**
  - ![](../images/screenshot_1615280802.png)

Verificamos que podemos hacer ping entre las dos máquinas, consultamos las ips con la orden **"ip a"**

desde fedora a rescue

![](../images/screenshot_1615281174.png)

desde rescue a fedora

![](../images/screenshot_1615282804.png)


Arrancamos con un system rescue cd y permitimos el acceso por ssh:

```bash
iptables -P INPUT ACCEPT
iptables -F
```

Nos ponemos un password de root para poder entrarle desde otra máquina:

```
passwd
```

Consultamos la ip:

```
ip a 
```

Nos informa que la ip es la 10.200.241.97 y ahora desde otra máquina ya podemos entrar y copiar/pegar comandos... 

Desde un escritorio virtual de fedora por ejemplo podemos conectarnos haciendo:

```
ssh root@10.200.241.97
```

Vamos a crear una partición de 100MB, vamos a formatearla, vamos a descargar unas fotos dentro y nos vamos a llevar una copia del disco a otro ordeandor:

```
#dejamos el disco sin particiones antiguas y creamos una nueva tabla de particiones
parted -s /dev/vda mklabel msdos

#hacemos una partición de 100MB
parted -s /dev/vda mkpart primary 4KB 100M

#formateamos la partición con le sistema de ficheros ntfs
mkfs.ntfs /dev/vda1

#montamos la unidad en un directorio para poder escribir dentro:
mkdir /mnt/test
mount /dev/vda1 /mnt/test

#descargar las imágenes
cd /mnt/test
wget https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Circo_cotatuero.jpg/800px-Circo_cotatuero.jpg
wget https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Pyrenees_topographic_map-es.svg/800px-Pyrenees_topographic_map-es.svg.png
```

Ahora imaginémonos que tenemos una máquina que nos han dado y que nos piden que recuperemos la información sin que arranque el pc, sin que el disco duro se vea modificado por nada. Eso se llama hacer un análisis forense del disco. También es útil no solo para extraer información sin arrancar el sistema operativo que lleve la máquina, si no que también es útil cuando tenemos un disco que está en mal estado, igual arranca una vez, pero se queda a medias, si trabajamos con él tenemos muchas probabilidades de terminar de estropearlo. 

Para hacer eso, queremos copiar bit a bit y de forma secuencial (eso es lo que dañará menos al disco). Para eso tenemos la herramienta **dd** que nos permite hacer una copia secuencial del disco. 



Esa copia la podemos hacer sobre un disco externo conectado por usb, o a través de la red.

Para terminar de simular el análisis forense, lo que haremos será reiniciar la máquina con un rescue cd de nuevo, como si nos fuéramos a encontrar con un disco que no sabemos que tiene y que queremos copiar bit a bit.



Una vez hemos reiniciado el rescue cd con el disco donde están las fotos que queremos auditar. miraríamos con un lsblk si tiene particiones:

```
[root@sysrescue ~]# lsblk 
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0    7:0    0 637.5M  1 loop /run/archiso/sfs/airootfs
sr0     11:0    1   699M  0 rom  /run/archiso/bootmnt
vda    254:0    0    60G  0 disk 
└─vda1 254:1    0  95.4M  0 part 
```

Hemos encontrado un disco con una partición vda1 de 95,4 MB.

La queremos recuperar sobre un directorio montado por sshfs. En otra máquina creamos ese directorio y le damos permisos a un usuairo para que pueda escribir en él. 

```
sudo mkdir /opt/forensic
sudo chown isard /opt/forensic/
```

Ahora desde el rescue cd nos montamos esa carpeta remota:

```
mkdir /mnt/forensic
sshfs isard@10.200.241.46:/opt/forensic /mnt/forensic
```

Ahora ya tenemos disponible en /mnt/forensic del rescue cd el acceso a la carpeta /opt/forensic del equipo remoto (en nuestro caso un fedora 32 de isard donde haremos el análisis forense)

Ya podemos realizar la copia con dd:

- if: dispositivo de entrada
- of: dispositivo de salida (puede ser un fichero)
- bs: (block size) tamaño del bloque de copia, irá leyendo bloques de tamaño bs y escribiendo bloques de tamaño bs. Ineteresa que sea un poco grande para que la copia se haga más rápido y dañe menos el disco, un tamaño de 1024 o 2048 está bien.
- status: si le ponemos progress me indica la progresión de la copia, para tener un poco de feedbak por pantalla

```
dd if=/dev/vda1 of=/mnt/forensic/vda1.raw bs=1024
```

Ahora disponemos en el fedora de una copia bit a bit de la partición original

```
[isard@f32-isard ~]$ ls -lah /opt/forensic/
total 96M
drwxr-xr-x. 2 isard root  4.0K Mar  9 09:08 .
drwxr-xr-x. 4 root  root  4.0K Mar  9 09:01 ..
-rw-r--r--. 1 isard isard    0 Mar  9 09:05 prova
-rw-r--r--. 1 isard isard  96M Mar  9 09:09 vda1.raw

```

Ahora vamos a montar en fedora ese disco bruto para recuperar las fotos o el contenido que hubiese dentro:

```
# creamos un directorio para el rescate
sudo mkdir /mnt/rescat
sudo mount -o ro,loop /opt/forensic/vda1.raw /mnt/rescat
```

la orden mount tiene las siguientes opciones que se hacen con **-o**:

- **ro**: read only (sólo se puede leer, no escribir)

- **loop**: monta un fichero como si fuese un dispositivo, es un tipo de montaje especial, que no busca un dispositivo físico, si no que simula la lectura del fichero como si fuese un dispositivo

  

En este caso como ha encontrado un tipo sistema de ficheros conocido (ntfs) ha sido capaz de montarlo y hemos podido recuperar las fotos

```
[isard@f32-isard ~]$ ls -lah /mnt/rescat/
total 784K
drwxrwxrwx. 1 root root 4.0K Mar  9 08:51 .
drwxr-xr-x. 3 root root 4.0K Mar  9 09:12 ..
-rwxrwxrwx. 1 root root 114K Aug 28  2020 800px-Circo_cotatuero.jpg
-rwxrwxrwx. 1 root root 657K Nov  5  2013 800px-Pyrenees_topographic_map-es.svg.png

```

y ahora ya las podemos mover a mi home por ejemplo:

```
cp -a /mnt/rescat /home/isard/rescat

```

Si tenemos un disco que no reconoce particiones y que sabemos que hay fotos o documentos que recuperar dentro de ese disco, sobre esa copia "raw" (en bruto) que hemos hecho con dd, podemo correr el programa photorec.

Para instalar photorec en fedora:

```
dnf -y install testdisk
```

Consultamos la ayuda:

```
[isard@f32-isard ~]$ photorec --help
PhotoRec 7.1, Data Recovery Utility, July 2019
Christophe GRENIER <grenier@cgsecurity.org>
https://www.cgsecurity.org

Usage: photorec [/log] [/debug] [/d recup_dir] [file.dd|file.e01|device]
       photorec /version

/log          : create a photorec.log file
/debug        : add debug information

PhotoRec searches for various file formats (JPEG, Office...). It stores files
in the recup_dir directory.

```

Vemos en la ayuda de photorec que le podemo pasar como parámetro un fichero .dd (o raw, que es lo mismo, un fichero extraído con dd). Lo queremos recuperar sobre un directorio de mi home. 

```
mkdir ~/recuperacio_photorec
photorec /d ~/recuperacio_photorec /opt/forensic/vda1.raw
```

Copio os pantallazos de photorec:

paso 1

```
PhotoRec 7.1, Data Recovery Utility, July 2019
Christophe GRENIER <grenier@cgsecurity.org>
https://www.cgsecurity.org

  PhotoRec is free software, and
comes with ABSOLUTELY NO WARRANTY.

Select a media (use Arrow keys, then press Enter):
>Disk /opt/forensic/vda1.raw - 99 MB / 95 MiB (RO)





>[Proceed ]  [  Quit  ]

```

paso2:

```
PhotoRec 7.1, Data Recovery Utility, July 2019
Christophe GRENIER <grenier@cgsecurity.org>
https://www.cgsecurity.org

Disk /opt/forensic/vda1.raw - 99 MB / 95 MiB (RO)

     Partition                  Start        End    Size in sectors
      Unknown                  0   0  1   193  12  6     195306 [Whole disk]
>   P NTFS                     0   0  1   193  12  6     195306




>[ Search ]  [Options ]  [File Opt]  [  Quit  ]

```

paso 3:

```
PhotoRec 7.1, Data Recovery Utility, July 2019
Christophe GRENIER <grenier@cgsecurity.org>
https://www.cgsecurity.org

   P NTFS                     0   0  1   193  12  6     195306

To recover lost files, PhotoRec needs to know the filesystem type where the
file were stored:
 [ ext2/ext3 ] ext2/ext3/ext4 filesystem
>[ Other     ] FAT/NTFS/HFS+/ReiserFS/...

```

paso 4:

```
PhotoRec 7.1, Data Recovery Utility, July 2019
Christophe GRENIER <grenier@cgsecurity.org>
https://www.cgsecurity.org

   P NTFS                     0   0  1   193  12  6     195306


Please choose if all space needs to be analysed:
 [   Free    ] Scan for file from NTFS unallocated space only
>[   Whole   ] Extract files from whole partition

```

paso 5:

```
PhotoRec 7.1, Data Recovery Utility, July 2019
Christophe GRENIER <grenier@cgsecurity.org>
https://www.cgsecurity.org

Disk /opt/forensic/vda1.raw - 99 MB / 95 MiB (RO)
     Partition                  Start        End    Size in sectors
   P NTFS                     0   0  1   193  12  6     195306


2 files saved in /home/isard/recuperacio_photorec directory.
Recovery completed.

You are welcome to donate to support and encourage further development
https://www.cgsecurity.org/wiki/Donation



[ Quit ]

```

Salimos del photorec y hemos encontrado las dos fotos:

```
[isard@f32-isard ~]$ ls -lah ~/recuperacio_photorec.1/
total 792K
drwxrwxr-x.  2 isard isard 4.0K Mar  9 09:27 .
drwx------. 17 isard isard 4.0K Mar  9 09:27 ..
-rw-rw-r--.  1 isard isard 657K Mar  9 09:27 f0101752.png
-rw-rw-r--.  1 isard isard 114K Mar  9 09:27 f0134528.jpg
-rw-rw-r--.  1 isard isard 5.2K Mar  9 09:27 report.xml

```

