Anar a la web de isard, a la part de [administració](https://isard.escoladeltreball.org/isard-admin/login/inf):  https://isard.escoladeltreball.org/isard-admin/login/inf

Et valides amb la teva compte de LDAP de la Escola (la que fas servir al moodle)

Amb aquesta pantalla fas clic a **Add new**:

![isard admin inici alumne sense escriptoris creats](../img/mainboard_1607677316.png)

Al formulari per afegir un nou escriptori has de seleccionar la plantilla escribint "rescue", seleccionar fent un clic a la fila de la plantilla i que apareixi la frase **Template selected: system rescue cd v1** tal i com es veu amb aquesta pantalla:

![](../img/mainboard_1607677444.png)

Ara cal donar un nom a l'escriptori, per exemple **rescue1** i fer un clic al botó **Create desktop**

![](../img/mainboard_1607677511.png)

Ara cal afegir la ISO del cd de rescue cd a l'escriptori. Cal fer clic al botó amb el simbol **+**  ![](../img/mainboard_1607677688.png) per desplegar la informació de l'escriptori i fer clic al botó **Edit**:

![](../img/mainboard_1607677713.png)

Ara cal desplegar l'apartat de **media** :

![](../img/mainboard_1607677757.png)

i apareix un camp per seleccionar **CD/DVD ISO**:

![](../img/mainboard_1607677805.png)

Ara comences a escriure rescue "system" i pots seleccionar la iso:

![](../img/mainboard_1607677858.png)

Ara la pantalla té la iso sleccionarda, el dispositiu de boot a CD/dvd. Ara podem canviar la xarxa per poder accedir a la vlan 241, que ens permetrà accedir des de la xarxa del departament a la nostra máquina virtual que estem creant:
![](../img/mainboard_1607678024.png)

Ara podem comprobar qut tot està bé (boot, networks, iso) i fer un clic al boó e modify desktop:



![](../img/mainboard_1607678075.png)

Ara a la pantalla principal de desktops apareix una rodona a la columna media, aixó ens indica que la ISO està conectada:

![](../img/mainboard_1607678140.png)

Ara ja podem fer start i ens surt la opció de obrir un visor amb **spice application** o amb **vnc browser**.

![](../img/mainboard_1607678719.png)

Amb vnc browser podem interactuar amb l'escriptori des de el navegador (cal acceptar que pugui obrir pestanyes). 

Es preferible fer servir el client del protocol spice, cal tenir instalat a un linux el paquet virt-install, que afegeix al sistema el programa remote-viewer que és el visor que fem servir per obrir el fitxer vv que es descarrega desde isard. Si no el tens instalat cal fer:

```
sudo dnf -y install virt-viewer
```

Si estàs amb un ordinador amb un windows tens un instal·lador a: https://virt-manager.org/download/ Cal descarregar i instal·lar el fitxer **msi**  https://virt-manager.org/download/sources/virt-viewer/virt-viewer-x64-9.0.msi



Amb el virt-viewer instal·lat ja podem fer clic a ![](../img/mainboard_1607678742.png)

Es descarregar un fitxer vv a la nostra carpeta de descàrregues del navegador:

![](../img/mainboard_1607678821.png)

Ara obrim el fitxer que estarà associat al remote-viewer i ja tenim accés a l'escriptori

![](../img/mainboard_1607678895.png):



Ara esperem 75 segons o millor fem clic dins de l'escriptori i li donem al botó de enter. Ara inicia la càrrega del kernel del sistema operatiu linux que hi ha a la ISO del system rescue cd. Aquesta distribució de linux ja està pensada per fer reparacions i diagnóstics amb moltes eines preinstalades. Entre d'altres el gparted que necessitem per aquesta práctica. Per defecte no tenim mode gràfic, llavors podem escriure startx per iniciar el mode gràfic:

![](../img/mainboard_1607679063.png)

I ara ja podem accedir al botó del gparted: 

![](../img/mainboard_1607679120.png)

I apareix el programa gparted amb un disc dur de 20GB on podem fer particions i treballar amb ell:

![](../img/mainboard_1607679158.png)

